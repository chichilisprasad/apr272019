﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel
{
    public class ARTReport
    {
        
        public Nullable<int> NewCasesSharedbyART { get; set; }

        public Nullable<int> CasesCarriedForward { get; set; }

        public Nullable<int> CasesContactedThOutreach { get; set; }

        public Nullable<int> AgreedtoVisitARTCenter { get; set; }

        public Nullable<int> PhysicallyTakentoART { get; set; }

        public Nullable<int> Death { get; set; }

        public Nullable<int> ARTatOtherNacoARTCenter { get; set; }

        public Nullable<int> OptedOutoftheProgram { get; set; }

        public Nullable<int> IncorrectAddress { get; set; }

        public Nullable<int> Migrated { get; set; }

        public Nullable<int> TransferredOut { get; set; }

        public Nullable<int> Alternatemadicine { get; set; }

        public Nullable<int> TakingPrivateARTTreatment { get; set; }

        public Nullable<int> Others { get; set; }

        public Nullable<int> ReportedbacktoARTCenter { get; set; }

    }
}
Modifying this file1 second time
